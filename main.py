from classes import text
from classes.game import Person
from classes.game import BColors


magic = [{"name": BColors.FAIL + "Fire" + BColors.ENDC, "cost": 10, "dmg": 60},
         {"name": BColors.WARNING + "Lightning" + BColors.ENDC, "cost": 14, "dmg": 70},
         {"name": BColors.OKBLUE + "Blizzard" + BColors.ENDC, "cost": 6, "dmg": 50}]

player = Person(460, 65, 60, 34, magic, "Player")
enemy = Person(1200, 65, 45, 25, magic, "Enemy")

running = True

text.enemy_attack()

while running:
    print("=======================")
    text.print_stat("Player HP", player.get_hp())
    text.print_stat("Player MP", player.get_mp())
    text.print_stat("Enemy HP", enemy.get_hp())
    text.print_stat("Enemy MP", enemy.get_mp())
    print("=======================")
    player.choose_action()
    choice = input("Choose action:")
    index = int(choice)-1

    # print("You chose", player.get_spell_name(int(index)))

    if index == 0:  # standard attack
        dmg = player.generate_damage();
        enemy.take_damage(dmg)
        text.attack_text("Player", dmg)

    elif index == 1:  # magic attack
        player.choose_magic()
        player_magic_choice = input("Choose spell:")
        index = int(player_magic_choice)-1

        if player.get_mp() < player.get_spell_mp_cost(index):
            text.no_magic()
        else:
            player.reduce_mp(player.get_spell_mp_cost(index))
            dmg = player.generate_spell_damage(index)
            enemy.take_damage(dmg)
            text.magic_attack_text("Player", player.get_spell_name(index), dmg)

    if enemy.get_hp() == 0:
        text.defeated_enemy()
        running = False
    else:
        enemy_choice = 1
        dmg = enemy.generate_damage()
        player.take_damage(dmg)
        text.attack_text("Enemy", dmg)

        if player.get_hp() == 0:
            text.dead()
            running = False

# player.choose_action()
# player.choose_magic()
