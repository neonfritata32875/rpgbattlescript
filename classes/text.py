from classes.game import BColors


def enemy_attack():
    print(BColors.FAIL + BColors.BOLD + "AN ENEMY ATTACKS!" + BColors.ENDC)


def attack_text(who, dmg):
    print(who,"attacked for", dmg, "points of damage.")


def magic_attack_text(who, spell, dmg):
    print(who, "cast", spell, "for", dmg, "points of damage.");


def print_stat(statname, value):
    print(BColors.BOLD + statname + ":" + str(value) + BColors.ENDC)


def defeated_enemy():
    print(BColors.OKGREEN + BColors.BOLD + "You defeated the enemy!" + BColors.ENDC)


def dead():
    print(BColors.FAIL + BColors.BOLD + "You are dead!" + BColors.ENDC)


def no_magic():
    print(BColors.WARNING + BColors.BOLD + "Not enough magic!" + BColors.ENDC)
